from django.contrib import admin
from django.urls import path
from . import views

app_name = "app6"

urlpatterns = [
    path('landingPage/', views.landingPage, name=''),
    path('', views.redirecting, name='redirecting'),
]
