from django.shortcuts import render, redirect
from .models import SaveStatus
from .forms import StatusForm


# Create your views here.
def landingPage(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/landingPage/')
	else:
		form = StatusForm()

	statusNow = SaveStatus.objects.order_by('-date')
	return render(request, 'landingPage.html', {'form': form, 'status': statusNow})


def redirecting(request):
	return redirect('/landingPage/')
