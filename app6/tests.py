from django.test import TestCase, LiveServerTestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import landingPage, redirecting
from django.http import HttpRequest
from .models import SaveStatus
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import time

# Create your tests here.

class UnitTestStory6(TestCase):
	def test_landingPage_url_is_exist(self):
		response = Client().get('/landingPage/')
		self.assertEqual(response.status_code, 200)
		
	def test_home_url_is_notexist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_landingPage_function(self):
		response = resolve('/landingPage/')
		self.assertEqual(response.func, landingPage)

	def test_landingPage_using_landingPage_template(self):
		response = Client().get('/landingPage/')
		self.assertTemplateUsed(response, 'landingPage.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = landingPage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = landingPage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Status</title>', html_response)

	#Redirect landing page
	def test_landing_page_using_redirecting_function(self):
		response = resolve('/')
		self.assertEqual(response.func, redirecting)

	def test_landing_page_redirecting(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_landing_page_redirected_to_home(self):
		response = Client().get('/')
		self.assertRedirects(response, '/landingPage/')

	#SetUp and Models
	def setUp(self):
		SaveStatus.objects.create(status="test")

	def test_if_models_in_database(self):
		modelsObject = SaveStatus.objects.create(status="fine and good")
		count_Object = SaveStatus.objects.all().count()
		self.assertEqual(count_Object, 2)

	def test_if_StatusDB_status_is_exist(self):
		modelsObject = SaveStatus.objects.get(id=1)
		statusObj = SaveStatus._meta.get_field('status').verbose_name
		self.assertEqual(statusObj, 'status')

	#Forms
	def test_forms_input_html(self):
		form = StatusForm()
		self.assertIn('id="id_status', form.as_p())

	def test_forms_validation_blank(self):
		form = StatusForm(data={'status':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['status'], ["This field is required."])

	#forms in templates
	def test_forms_in_template(self):
		request = HttpRequest()
		response = landingPage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form method="POST" class="register_form">', html_response)

class FunctionalTestStory6(LiveServerTestCase):

	def setUp(self):
		self.option = webdriver.ChromeOptions()
		self.option.add_argument('dns-prefetch-disable')
		self.option.add_argument('no-sandbox')
		self.option.add_argument('headless')
		self.option.add_argument('disable-gpu')
		self.option.add_argument('disable-dev-shm-usage')
		self.option.add_argument('disable-extensions')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', options=self.option)
		# self.selenium = webdriver.Chrome()
		super(FunctionalTestStory6, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTestStory6, self).tearDown()

	def test_input_todo(self):
		
		self.browser.get(self.live_server_url+'/landingPage/')
		time.sleep(5)
		hello = self.browser.find_element_by_id("id_status")
		hello.click()
		hello.send_keys("test")
		butClick = self.browser.find_element_by_css_selector("button")
		butClick.click()
		time.sleep(3)




