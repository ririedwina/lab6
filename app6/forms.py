from django import forms
from .models import SaveStatus


class StatusForm(forms.ModelForm):
    status = forms.CharField(max_length=300, widget=forms.Textarea)

    class Meta:
    	model = SaveStatus
    	fields = ('status',)
